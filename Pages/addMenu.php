<?php

App::pageAuth([App::ROLE_USER]);

if(isset($_POST['name']))
{
    Menu::register($_POST);
}
?>

<div class="container">
    <div class="card card-model card-model-sm">
        <div class="card-header">
            Add Menu Item
        </div>
        <div class="card-body">
            <?= Menu::registerForm(); ?>
        </div>
    </div>
</div>
