<?php
App::pageAuth(["user"], "login");
$restaurant = Restaurant::findBy('id', $_GET['id'])[0];

$user = App::getUser();

$items = Menu::findBy('restaurant_id' , $_GET['id']);



?>

<div class="container">
    <div class="col-sm-8 text-left">
        <image class="img-rounded" src="images/<?php  echo ($restaurant->getRestaurantImage()); ?>"  ></image>
        <div class="float-right text-center">
            <i class="far fa-clock fa-3x"></i>
            <p><?php echo date('H:i' , strtotime($restaurant->open_at)) . ' - ' . date('H:i' , strtotime($restaurant->closed_at)) ; ?></p>
        </div>
        <h1><?php echo ucfirst($restaurant->getName()); ?></h1>
        <h4 class="font-weight-bold">Tel: <?php echo 0 , ucfirst($restaurant->getPhoneNumber()); ?></h4>
        <br>
        <div class="row">
            <div class="col-sm">Location: <?php echo ucfirst($restaurant->getCity(). " " . $restaurant->getZipcode() . " " . $restaurant->getStreetName()
            . " " . $restaurant->getStreetSuffix() . " " . $restaurant->getStreetNumber()); ?></div>
            <br>
            <div class="col-sm">E-mail: <?php echo ucfirst($restaurant->getEmail()); ?></div>
            <a class="btn btn-primary" <?= App::link('addMenu&restaurant_id='.$restaurant->id) ?>>Add menu item</a>
        </div>
        <hr>

        <?php foreach ($items as $item) { ?>
        <h2 style="color: cadetblue" class="font-weight-bold"><?php echo $item->getType()?>:</h2>
        <img src="images/<?php echo $item->getImage()?>">
        <h3><?php echo $item->getName()?></h3>
        <p><?php echo $item->getDescription()?></p>
        <div class="float-right"><?php echo '€' .$item->getPrice(); ?></div>
        <form action="" method="POST">
            <a class="btn btn-primary" name="order" <?= App::link('addOrder&restaurant_id='.$restaurant->id . '&item_id=' . $item->id) ?>>order<br/></a>
        </form>
        <?php } ?>

    </div>
</div>
