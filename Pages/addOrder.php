<?php

App::pageAuth([App::ROLE_USER]);

if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    Order::register($_POST, $_GET['item_id']);
}

?>

<div class="container">
    <div class="card card-model card-model-sm">
        <div class="card-header">
            shoppingcart
        </div>
        <div class="card-body">
            <?= Order::orderForm($_GET['restaurant_id'], $_GET['item_id']); ?>
        </div>
    </div>
</div>

<script>
    $('#quantity').change(function() {
        $('#subtotal').val($(this).val() * $(this).data('price'));
    });
</script>

