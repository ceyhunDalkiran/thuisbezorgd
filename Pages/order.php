<?php

$user = App::getUser();
$orders = Order::findBy("user_id", $user->id);

$item = DB::getInstance()->prepare("SELECT * FROM items WHERE id = :id");
$item->setFetchMode(PDO::FETCH_CLASS, 'Menu');
$item->execute(['id'=> $orders->item_id]);
$item = $item->fetch();
?>

<div class="container card-model-sm">
    <?php  foreach ($orders as $order){ ?>
        <div class="card w-75">
            <div class="card-body text-center">
                    <?php echo $item->name ,'€'. $order->subtotal,'<hr>'. date( 'D d M Y', strtotime($order->created_at));?>
            </div>
        </div>
    <?php } ?>
</div>