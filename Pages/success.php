<div class="container">
    <h2>Order</h2>
    <div class="panel panel-default">
        <h4 class="panel-heading text-success">Your order is succesfully placed!</h4>
        <br>
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Firstname</th>
                <th scope="col">Lastname</th>
                <th scope="col">Item name</th>
                <th scope="col">Quantity</th>
                <th scope="col">Total price</th>
                <th scope="col">Created at</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">1</th>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
                <td>price</td>
                <td>@mdo</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
