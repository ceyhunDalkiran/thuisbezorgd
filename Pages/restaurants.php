<?php

$restaurants = Restaurant::get();

if (isset($_POST['search']))
{
    $query = DB::getInstance()->prepare("SELECT * FROM restaurants WHERE open_at < NOW() AND closed_at > NOW() AND  name LIKE :search");
    $query->execute([
        'search' => '%'.$_POST['search'].'%',
   ]);
   $restaurants = $query->fetchAll(PDO::FETCH_CLASS, 'Restaurant');
}

?>

<div class="container">
    <form action="" method="POST">
        <input name="search" type="text" placeholder="Restaurant name"><br><br>
    </form>
        <div id="users">
            <?php foreach ($restaurants as $resultRestaurant) { ?>
                <a class="disabled"<?= App::link('restaurant&id='.$resultRestaurant->id) ?>>
                    <div class="panel panel-default">
                        <div class="panel-body font-weight-bold">
                            <div class="p-4"><image class="img-rounded" src="images/<?php echo $resultRestaurant->getRestaurantImage(); ?>"></image>
                            <?php echo ucfirst($resultRestaurant->getName());?>
                                <div class="text-right">
                                <?php if(time() > strtotime($resultRestaurant->getOpenAt()) && time()  < strtotime($resultRestaurant->getClosedAt())){ echo 'OPEN';}else{ echo "CLOSED";}?>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            <?php } ?>
        </div>
</div>
