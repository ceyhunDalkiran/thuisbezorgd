<?php
$orders = DB::getInstance()->prepare("SELECT * FROM orders");
$orders->execute();
$orders = $orders->fetchAll(PDO::FETCH_CLASS, 'Order');


foreach($orders as $order){
    $item = DB::getInstance()->prepare("SELECT * FROM items WHERE id = :id");
    $item->setFetchMode(PDO::FETCH_CLASS, 'Menu');
    $item->execute(['id'=> $order->item_id]);
    $item = $item->fetch();
    ?>
    <div class="container"><table class="table table-dark">
            <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?php echo $item->name; ?></td>
                <td><?php echo $order->subtotal; ?></td>
            </tr>
            </tbody>
        </table>
        <a class="btn btn-primary"' <?=  App::link("success")?>'>Checkout</a>
    </div>
<?php } ?>



