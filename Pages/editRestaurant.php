<?php

App::pageAuth(['user'], "login");

if (isset($_POST['email'])) {
    Restaurant::updateRestaurant($_POST);
}
?>

<div class="container">
    <div class="card card-model card-model-sm">
        <div class="card-header">
            Update
        </div>
        <div class="card-body">
            <?= Restaurant::editRestaurantForm(); ?>
        </div>
    </div>
</div>

