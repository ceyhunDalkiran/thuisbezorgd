<?php

$user = App::getUser();
$restaurants = Restaurant::findBy("user_id", $user->id);

?>

<div class="container card-model-sm">
    <?php  foreach ($restaurants as $restaurant){ ?>
        <div class="card w-75">
            <div class="card-body text-center">
                <a class="disabled"<?= App::link('restaurant&id='.$restaurant->id) ?>>
                <?php echo $restaurant->getName();?>
            </div>
        </div>
    <?php } ?>
</div>
