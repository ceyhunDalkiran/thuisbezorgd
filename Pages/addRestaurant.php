<?php

App::pageAuth([App::ROLE_USER]);

if(isset($_POST['name']))
{
    Restaurant::register($_POST);
}
?>

<div class="container">
    <div class="card card-model card-model-sm">
        <div class="card-header">
            Add Restaurant
        </div>
        <div class="card-body">
            <?= Restaurant::registerForm(); ?>
        </div>
    </div>
</div>
