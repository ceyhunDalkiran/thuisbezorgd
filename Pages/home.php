<?php

App::pageAuth(['user'], "login");

// Example to get user
$user = User::findById(1);

?>

<div class="container">

    <div class="card-deck">
        <a>
            <div class="card">
                <div class="card-body text-center">
                    <p class="card-text"><a <?= App::link('restaurants') ?> >order food</a></p>
                </div>
            </div>
        </a>

        <div class="card">
            <div class="card-body text-center">
                <p class="card-text"><a <?= App::link('addRestaurant') ?> >add restaurant</a></p>
            </div>
        </div>

    </div>
</div>
