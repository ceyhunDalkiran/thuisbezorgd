<?php

use Intervention\Image\ImageManagerStatic as Image;

class Menu extends Model
{

    protected $table = 'items';


    /**
     * @Type varchar(255)
     */
    protected $image;

    /**
     * @Type varchar(255)
     */
    protected $name;

    /**
     * @Type double(9,2)
     */
    protected $price;

    /**
     * @Type varchar(255)
     */
    protected $description;

    /**
     * @Type int(11)
     */
    protected $restaurant_id;


    /**
     * @Type int(11)
     */
    protected $type;


    public function __construct()
    {

    }


    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getCreatedAt()
    {
        return date( "d/m/Y", strtotime($this->created_at));
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getType()
    {
        switch ($this->type){
            case 0:
                return "Drank";
                break;
            case 1:
                return "Voorgerecht";
                break;
            case 2:
                return "Hoofdgerecht";
                break;
        }

    }


    protected static function newModel($obj)
    {

        $email = $obj->email;

        $existing = Menu::findBy('email', $email);
        if(count($existing) > 0) return false;

        //Check if user is valid
        return true;

    }


    public static function register($form)
    {
        if(isset($_SESSION['errors']) && count($_SESSION['errors'])) {
            return false;
        }

        $menu = new Menu();
        $menu->name = $form['name'];
        $menu->description = $form['description'];
        $menu->price = $form['price'];
        $menu->restaurant_id =  $_GET['restaurant_id'];
        $menu->type = $form['type'];

        if ( !!$_FILES['image']['tmp_name']) {
            $fileParts = pathinfo($_FILES['image']['name']);

            if($menu->image) {
                @unlink(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$menu->image);
            }

            $menu->image = sha1($fileParts['filename'].microtime()).'.'.$fileParts['extension'];

            if(in_array($fileParts['extension'], ['jpg', 'jpeg', 'png'])) {
                if(move_uploaded_file($_FILES['image']['tmp_name'], Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$menu->image)) {
                    // the file has been moved correctly

                    // open an image file
                    $img = Image::make(Http::$dirroot.'public/images/'.$menu->image);

                    // now you are able to resize the instance
                    $img->resize(200, 200);

                    // finally we save the image as a new file
                    $img->save(Http::$dirroot.'public/images/'.$menu->image,100);
                }
            }
            else {
                // error this file ext is not allowed
            }
        }
        $menu->save();
    }

    public static function updateMenu($form)
    {
        $menu = self::findById(App::$user->id);
        $menu->name = $form['firstname'];
        $menu->description = $form['lastname'];
        $menu->price = $form['lastname'];
        $menu->type = $form['type'];

        if ( !!$_FILES['image']['tmp_name']) {
            $fileParts = pathinfo($_FILES['image']['name']);

            if($menu->image) {
                @unlink(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$menu->image);
            }

            $menu->image = sha1($fileParts['filename'].microtime()).'.'.$fileParts['extension'];

            if(in_array($fileParts['extension'], ['jpg', 'jpeg', 'png'])) {
                if(move_uploaded_file($_FILES['image']['tmp_name'], Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$menu->image)) {
                    // the file has been moved correctly, now resize it

                    // open and resize an image file
                    $img = Image::make(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$menu->image)->fit(300, 200);

                    // save file as jpg with maximum quality
                    $img->save(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$menu->image, 100);

                }
            }
            else {
                // error this file ext is not allowed
            }
        }

        $menu->save();
    }

    public static function registerForm()
    {
        $form = new Form();

        $form->addField((new FormField("name"))
            ->placeholder("Menu item name")
            ->required());

        $form->addField((new FormField("description"))
            ->placeholder("Menu item description")
            ->required());

        $form->addField((new FormField("price"))
            ->type("double")
            ->placeholder("price")
            ->required());


        $form->addField((new FormField("image"))
            ->type("file")
            ->placeholder("Image")
            ->required());

        $form->addField((new FormField("type"))
            ->type("select")
            ->placeholder("Category type")
            ->value(["Drank" , "Voorgerecht" , "Hoofdgerecht"])
            ->required());

        $form->addField((new FormField("restaurant_id"))
            ->type("hidden")
            ->value($_GET['restaurant_id']));


        return $form->getHTML();
    }


    public static function editMenuForm()
    {
        $menu = Restaurant::findById(App::$user->id);

        $form = new Form();

        $form->addField((new FormField("name"))
            ->placeholder("Menu item name")
            ->value($menu->name)
            ->required());

        $form->addField((new FormField("image"))
            ->type("file")
            ->placeholder("Image")
            ->value($menu->image));

        $form->addField((new FormField("description"))
            ->placeholder("Menu item description")
            ->value($menu->description)
            ->required());

        $form->addField((new FormField("price"))
            ->placeholder("price")
            ->value($menu->price)
            ->required());

        return $form->getHTML();
    }

}
