<?php

class Order extends Model
{

    protected $table = 'orders';

    /**
     * @Type int(11)
     */
    protected $item_id;

    /**
     * @Type int(11)
     */
    protected $restaurant_id;

    /**
     * @Type int(11)
     */
    protected $subtotal;

    /**
     * @Type int(11)
     */
    protected $user_id;

    /**
     * @Type int(11)
     */
    protected $quantity;


    public function __construct()
    {

    }


    public function getSubtotal()
    {
        return $this->subtotal;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function getRestaurantId()
    {
        return $this->restuarant_id;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    protected static function newModel($obj)
    {
        //Check if user is valid
        return true;
    }

    public static function register($form,$item)
    {

        if(isset($_SESSION['errors']) && count($_SESSION['errors'])) {
            return false;
        }
        $item = Menu::findById($item);
        $order = new Order();
        $order->user_id = App::$user->id;
        $order->item_id = $_GET['item_id'];
        $order->restaurant_id = $_GET['restaurant_id'];
        $order->subtotal = $form['quantity'] * $item->price;
        $order->save();

        return $order;
    }

    public static function updateShoppingcart($form)
    {
        $restaurant = self::findById(App::$user->id);
        $restaurant->quantity = $form['quantity'];
        $restaurant->save();
    }

    public static function orderForm($restaurantId, $item)
    {
        $item = Menu::findById($item);
        $form = new Form();

        $form->addField((new FormField("item_id"))
            ->type('hidden')
            ->value($item->id));

        $form->addField((new FormField("restaurant_id"))
            ->type('hidden')
            ->value($restaurantId));

        $form->addField((new FormField("quantity"))
            ->type('number')
            ->placeholder("quantity")
            ->dataprice($item->price)
            ->required());

        $form->addField((new FormField("subtotal"))
            ->placeholder("subtotal")
//            ->value($subtotal)
            ->required());

        return $form->getHTML();
    }

    public static function editOrderForm()
    {

        $form = new Form();

        $form->addField((new FormField("quantity"))
            ->type('number')
            ->placeholder("quantity")
            ->required());

        return $form->getHTML();
    }
}