<?php

use Intervention\Image\ImageManagerStatic as Image;

class Restaurant extends Model
{

    protected $table = 'restaurants';

    /**
     * @Type varchar(255)
     */
    protected $name;

    /**
     * @Type varchar(255)
     */
    protected $email;

    /**
     * @Type varchar(255)
     */
    protected $zipcode;

    /**
     * @Type varchar(255)
     */
    protected $city;

    /**
     * @Type int(12)
     */
    protected $phone_number;

    /**
     * @Type varchar(255)
     */
    protected $street_name;

    /**
     * @Type int(11)
     */
    protected $street_number;

    /**
     * @Type varchar(255)
     */
    protected $image;

    /**
     * @Type varchar(255)
     */
    protected $street_suffix;

    /**
     * @Type int(11)
     */
    protected $user_id;

    /**
     * @Type varchar(8)
     */
    protected $open_at;

    /**
     * @Type varchar(8)
     */
    protected $closed_at;

    public function __construct()
    {

    }


    public function getName()
    {
        return $this->name;
    }

    public function getOpenAt()
    {
        return $this->open_at;
    }

    public function getClosedAt()
    {
        return $this->closed_at;
    }

    public function getZipcode()
    {
        return $this->zipcode;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getRestaurantImage()
    {
        return $this->image;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getStreetName()
    {
        return $this->street_name;
    }

    public function getStreetNumber()
    {
        return $this->street_number;
    }

    public function getPhoneNumber()
    {
        return $this->phone_number;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getStreetSuffix()
    {
        return $this->street_suffix;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    protected static function newModel($obj)
    {

        $email = $obj->email;

        $existing = Restaurant::findBy('email', $email);
        if(count($existing) > 0) return false;

        //Check if user is valid
        return true;

    }

    public static function register($form)
    {

        if(isset($_SESSION['errors']) && count($_SESSION['errors'])) {
            return false;
        }

        $restaurant = new Restaurant();
        $restaurant->name = $form['name'];
        $restaurant->email = $form['email'];
        $restaurant->phone_number = $form['phone_number'];
        $restaurant->zipcode = $form['zipcode'];
        $restaurant->city = $form['city'];
        $restaurant->street_name = $form['street_name'];
        $restaurant->street_number = $form['street_number'];
        $restaurant->street_suffix = $form['street_suffix'];
        $restaurant->image = "standardRes.jpg";
        $restaurant->user_id = App::$user->id;
        $restaurant->open_at = $form['open_at'];
        $restaurant->closed_at = $form['closed_at'];
        $restaurant->save();
    }

    public static function updateRestaurant($form)
    {
        $restaurant = self::findById(App::$user->id);
        $restaurant->name = $form['name'];
        $restaurant->zipcode = $form['zipcode'];
        $restaurant->email = $form['email'];
        $restaurant->phone_number = $form['phone_number'];
        $restaurant->city = $form['city'];
        $restaurant->street_name = $form['street_name'];
        $restaurant->street_number = $form['street_number'];
        $restaurant->street_suffix = $form['street_suffix'];

        if ( !!$_FILES['image']['tmp_name']) {
            $fileParts = pathinfo($_FILES['image']['name']);

            if($restaurant->image) {
                @unlink(Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$restaurant->image);
            }

            $restaurant->image = sha1($fileParts['filename'].microtime()).'.'.$fileParts['extension'];

            if(in_array($fileParts['extension'], ['jpg', 'jpeg', 'png'])) {
                if(move_uploaded_file($_FILES['image']['tmp_name'], Http::$dirroot.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$restaurant->image)) {
                    // the file has been moved correctly

                    // open an image file
                    $img = Image::make(Http::$dirroot.'public/images/'.$restaurant->image);

                    // now you are able to resize the instance
                    $img->resize(200, 200);

                    // finally we save the image as a new file
                    $img->save(Http::$dirroot.'public/images/'.$restaurant->image,100);
                }
            }
            else {
                // error this file ext is not allowed
            }
        }
        $restaurant->save();
    }

    public static function registerForm()
    {
        $form = new Form();

        $form->addField((new FormField("name"))
            ->placeholder("restaurant name")
            ->required());

        $form->addField((new FormField("email"))
            ->placeholder("E-mail")
            ->required());

        $form->addField((new FormField("phone_number"))
            ->type("number")
            ->placeholder("phone number")
            ->required());

        $form->addField((new FormField("zipcode"))
            ->placeholder("zipcode")
            ->required());

        $form->addField((new FormField("city"))
            ->placeholder("city")
            ->required());

        $form->addField((new FormField("street_name"))
            ->placeholder("street name")
            ->required());

        $form->addField((new FormField("street_number"))
            ->type("number")
            ->placeholder("street number")
            ->required());

        $form->addField((new FormField("street_suffix"))
            ->placeholder("street suffix"));

        $form->addField((new FormField("open_at"))
            ->type("time")
            ->placeholder("opening time")
            ->required());

        $form->addField((new FormField("closed_at"))
            ->type("time")
            ->placeholder("closing time")
            ->required());

        return $form->getHTML();
    }

    public static function editRestaurantForm()
    {
        $restaurant = Restaurant::findById(App::$user->id);

        $form = new Form();

        $form->addField((new FormField("name"))
            ->placeholder("Name")
            ->value($restaurant->name)
            ->required());

        $form->addField((new FormField("email"))
            ->type("email")
            ->placeholder("E-mail")
            ->value($restaurant->email)
            ->required());

        $form->addField((new FormField("phone_number"))
            ->placeholder("Phone number")
            ->value($restaurant->phone_number)
            ->required());

        $form->addField((new FormField("image"))
            ->type("file")
            ->placeholder("Image")
            ->value($restaurant->image));


        $form->addField((new FormField("zipcode"))
            ->placeholder("Zipcode")
            ->value($restaurant->zipcode)
            ->required());

        $form->addField((new FormField("city"))
            ->placeholder("City")
            ->value($restaurant->city)
            ->required());

        $form->addField((new FormField("street_name"))
            ->placeholder("Street name")
            ->value($restaurant->street_name)
            ->required());

        $form->addField((new FormField("street_number"))
            ->placeholder("Street number")
            ->value($restaurant->street_number)
            ->required());

        $form->addField((new FormField("street_suffix"))
            ->placeholder("Street suffix")
            ->value($restaurant->street_suffix)
            ->required());

        return $form->getHTML();
    }
}