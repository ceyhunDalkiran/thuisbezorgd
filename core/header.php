<?php
//Create your menu here
?>
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <h5 class="my-0 mr-md-auto font-weight-normal">Company name</h5>
        <?php if(App::checkAuth(App::ROLE_USER)){ ?>
            <a class="p-2 text-dark" <?= App::link('home') ?>>Home</a>
            <a class="p-2 text-dark" <?= App::link('restaurants') ?>>Order Food</a>
            <a class="p-2 text dark" <?= App::link('shoppingcart')?>>Shoppingcart</a>
            <li class="nav-item dropdown" style="list-style-type: none;">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Restaurant
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" <?= App::link('addRestaurant') ?>>Add</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" <?= App::link('chooseRestaurant') ?>>Edit</a>
                </div>
            </li>
        <?php } ?>

        <?php if(App::checkAuth(App::ROLE_GUEST)){?>
            <a class="p-2 text-dark" <?= App::link('login') ?>>Login</a>
            <a class="p-2 text-dark" <?= App::link('register') ?>>Register</a>
        <?php } else { ?>
            <a class="p-2 text-dark" <?= App::link('logout') ?>>Logout</a>
        <?php } ?>
    </nav>
    <!-- <a class="btn btn-outline-primary" href="#">Sign up</a> -->
</div>
